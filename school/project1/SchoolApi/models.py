from django.db import models

# Create your models here.
#Admin model
class Admin(models.Model): 
    name = models.CharField(max_length=50, unique= True, null=False)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=100)
    
#Teacher model
class Teacher(models.Model):
    teacheremp_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=100, null = False, unique=True)
    designation = models.CharField(max_length=100, null=False)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=8)

#subject model
class Subject(models.Model):
    name = models.CharField(max_length=100, unique=True, null=True)

#class model
class Class(models.Model):
    name = models.IntegerField(unique= True,null= True)

#assigning class to teacher model
class AssignClass(models.Model):
    teacher_id = models.ForeignKey(Teacher, related_name='teacher', on_delete= models.CASCADE)
    subject_id = models.ForeignKey(Subject, related_name='subject', on_delete= models.CASCADE)
    class_id = models.ForeignKey(Class, related_name='class1', on_delete= models.CASCADE)

#homework model
class Homework(models.Model):
    class_id = models.ForeignKey(Class, related_name='class2', on_delete= models.CASCADE)
    subject_id = models.ForeignKey(Subject, related_name='subject2', on_delete= models.CASCADE)
    homework = models.CharField(max_length=250, null = False)

#noticeboard model
class Noticeboard(models.Model):
    notice = models.ImageField(upload_to = 'images/', max_length= 100, null = False)  #python -m pip install Pillow

#leave request model
class Leave(models.Model):
    teacheremp_id = models.ForeignKey(Teacher, related_name='teacher3', on_delete=models.CASCADE)
    reason = models.CharField(max_length=200, null=False)
    date = models.DateField(max_length=30)
    code = models.CharField(max_length=15)
    status = models.CharField(max_length=15)

