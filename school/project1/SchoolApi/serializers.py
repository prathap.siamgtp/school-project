from dataclasses import fields
from http.client import ImproperConnectionState
from rest_framework import serializers
from .models import *

class AdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Admin
        fields = "__all__"

class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = "__all__"

class SubSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = "__all__"

class ClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = Class
        fields = "__all__"

class AssignClassSerializer (serializers.ModelSerializer):
    class Meta:
        model = AssignClass
        fields = "__all__"

class HomeworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Homework
        fields = "__all__"   

class NoticeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Noticeboard
        fields = "__all__"   

class LeaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Leave
        fields = "__all__"