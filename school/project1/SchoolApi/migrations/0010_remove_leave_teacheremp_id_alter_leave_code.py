# Generated by Django 4.0.6 on 2022-08-02 07:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SchoolApi', '0009_leave'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='leave',
            name='teacheremp_id',
        ),
        migrations.AlterField(
            model_name='leave',
            name='code',
            field=models.CharField(max_length=15),
        ),
    ]
