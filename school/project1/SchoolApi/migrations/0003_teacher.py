# Generated by Django 4.0.6 on 2022-08-01 09:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SchoolApi', '0002_alter_admin_password'),
    ]

    operations = [
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('teacheremp_id', models.IntegerField(unique=True)),
                ('name', models.CharField(max_length=100)),
                ('designation', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=100)),
                ('password', models.CharField(max_length=8)),
            ],
        ),
    ]
