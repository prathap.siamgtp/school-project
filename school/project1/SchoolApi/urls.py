from django.urls import URLPattern, path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('adminreg/',AdminRegister.as_view()),
    path('adminlog/',AdminLogin.as_view()),
    path('teacherreg/',TeacherReg.as_view()),
    path('teacherlog/',TeacherLogin.as_view()),
    path('subject/',SubjectRegister.as_view()),
    path('subview/',SubjectView.as_view()),
    path('class/',ClassRegister.as_view()),
    path('classview/',ClassView.as_view()),
    path('assign/',AssignClassRegister.as_view()),
    path('assignview/',ViewAssignClass.as_view()),
    path('homework/', HomeworkRegister.as_view()),
    path('teacherpage/',TeacherPage1.as_view()),
    path('notice/', NoticeRegister.as_view()),
    path('leave/',LeaveRequest.as_view()),
    path('leaveall/',LeaveAllView.as_view()),
    path('leaveview/<str:pk>/',LeaveView.as_view()),
    path('leaveupdate/<str:pk>/',LeaveUpdate.as_view())
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)  #for fetch and view the image or file
