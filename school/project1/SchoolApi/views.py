from django.shortcuts import render
from rest_framework.views import APIView 
from rest_framework.response import Response
from validate_email import validate_email
#token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User

import random
import string
from rest_framework import authentication, permissions

#check date format
from datetime import datetime
#pagination
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
#password encryption
from django.contrib.auth.hashers import make_password
from django.contrib.auth.hashers import check_password

from .email import *
from .models import *
from .serializers import *
from .permissions import *
from .pagination import *
import re
#validate the password
check_pw =re.compile(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$")

# Create your views here.
#admin Register
class AdminRegister(APIView): 
    def get(self, request):
        detail = Admin.objects.all()
        serializer = AdminSerializer(detail,many = True)
        return Response(serializer.data)
    
    def post(self, request):
        serializer = AdminSerializer(data=request.data)
        password = request.data['password']
        email = request.data['email'] 
        name = request.data['name']
        if name.isalpha() and len(name)>2: #check the name is alphabets and more than 2 characters
            pass
            if re.fullmatch(check_pw,password): #validate the password
                pass
                if validate_email(email): #validate the email
                    pass
                    if serializer.is_valid(): #validate the data
                        encryptedpassword=make_password(password) #make encrypt password

                        Admin.objects.create(name = request.data['name'],
                                            email = request.data['email'],
                                            password = encryptedpassword)
                        return Response({"message":"admin created successfully",
                                        "details": serializer.data})
                    return Response(serializer.errors)
                return Response({"message": "email not valid"})
            return Response({"message":"password contain Minimum six characters, at least one uppercase letter, one lowercase letter, one number and one special character"})
        return Response({"message": "in name field must be more than two characters and alphabets only allowd"})

class AdminLogin(APIView): #admin login
    def post(self, request):

        request_data = {"name": request.data['name'],
                        "password":request.data['password']}
        s1 = request.data['name'] #get the request name, password
        s2 = request.data['password']
        try:
            user = Admin.objects.filter(name = s1).first() #fetch all details from the request name
            if check_password(s2,user.password):
                pass
            else:
                return Response({"message": "wrong  password"})
        except:
            return Response({"message": "wrong user name"})
        
        user1=User.objects.get_or_create(username = s1, is_superuser = True) #get or create the registered user in auth_user table
                
        user=User.objects.filter(username=s1).first()
        print(user)          
        token, created = Token.objects.get_or_create(user=user) #get or create the token
        return Response({"message":"login successfully",
                    "name":s1,                        
                    'token': token.key,
                })


#register the teacher only by admin
class TeacherReg(APIView):
    
    permission_classes = [IsAdmin] #permission only for admin
    def post(self, request):        
        request_data = {"teacheremp_id": request.data['teacheremp_id'],
                        "name":request.data['name'],
                        "designation":request.data['designation'],
                        "email": request.data['email'],
                        }
        serializer = TeacherSerializer(data=request_data)
        teacher_id1 = str(request.data['teacheremp_id'])
        name1 = request.data['name']
        designation1 = request.data['designation']
        email1 = request.data['email']
        characters = list(string.ascii_letters + string.digits + "!@#$%^&*()") #generate the password
        password1 = ''.join(random.choice(characters) for i in range(8))

        if name1.isalpha() and len(name1)>2: #validate the name
            pass
            if designation1.isalpha(): #validate the designation
                pass
                if teacher_id1.isdigit() and len(teacher_id1) == 4: #validate the teacher_emp id is numeric
                    pass
                    if validate_email(email1): #validate the email
                        pass
                        request_data["password"] = password1
                        serializer1 = TeacherSerializer(data=request_data)
                        if  serializer1.is_valid():
                            encryptpass = make_password(password1)
                            Teacher.objects.create(
                                teacheremp_id = teacher_id1,
                                name = name1,
                                designation = designation1,
                                email = email1,
                                password= encryptpass
                            )
                            mesg = {"name": name1,
                                    #"teacher_id": teacher_id1,
                                    "password": password1}
                            mail_mesg = (f"successfully created your teacher account!\n your name: {name1}\n password: {password1}")

                            send_mail(email1, mail_mesg) #send mail to teacher
                            return Response({"message": "created successfully",
                                            "details":mesg})
                        return Response(serializer1.errors)               
                    return Response({"message": "invalid email id!"})
                return Response({"message": "teacher id must be number and exact 4 characters!"})
            return Response({"message": "designation shoud contain alphabets only"})
        return Response({"message": "name must be more than two characters shoud contain alphabets only"})

 
class TeacherLogin(APIView): #login teacher

    def post(self, request):
        request_data = {"name": request.data['name'],
                        "password":request.data['password']}
        serializer = TeacherSerializer(data=request_data)
        s1 = request.data['name']
        s3 = request.data['password']
        try:
            user = Teacher.objects.filter(name = s1).first() #fetch all details from the request name
            if check_password(s3,user.password): #check the password
                pass
            else:
                return Response({"message": "wrong  password"})
        except:
            return Response({"message": "wrong user name"})
            
        user1=User.objects.get_or_create(username = s1,is_staff = True) #get or create the registered teacher in auth_user table         
        user=User.objects.filter(username=s1).first()
        print(user)          
        token, created = Token.objects.get_or_create(user=user) #get or create the token for teacher
        return Response({"message":"login successfully",
                    "name":s1,                        
                    'token': token.key,
                })


class SubjectRegister(APIView): #insert the subject by admin
    
    permission_classes = [IsAdmin] # set a permission for admin

    def post(self, request):
        serializer = SubSerializer(data=request.data)
        name = request.data['name']
        if name.isalpha() and len(name)>2:
            pass
            if serializer.is_valid(): #validate the data
                pass
                serializer.save() 
                return Response({"message":"subject created successfully",
                                "details":serializer.data})
            return Response(serializer.errors)
        return Response({"message":"in subject name field must be more than 2 characters and alphabets only allowed"})

class SubjectView(APIView): #view subject 
    permission_classes = [IsAdmin|IsTeacher] # set a permission for admin and teacher
    def get(self, request):
        detail = Subject.objects.all()
        serializer = SubSerializer(detail,many = True)
        return Response(serializer.data)

class ClassRegister(APIView): 
    permission_classes = [IsAdmin] # set a permission for admin

    def post(self, request): #insert the class only by admin
        serializer = ClassSerializer(data=request.data)
        class1 =str(request.data['name'])
        if class1.isdigit():
            pass
            if serializer.is_valid(): #validate the data
                pass
                serializer.save()   
                return Response({"message":"class added successfully",
                                "details":serializer.data})
            return Response(serializer.errors)
        return Response({"message":"class id must be numeric"})

class ClassView(APIView): #view class
    permission_classes = [IsAdmin | IsTeacher] # set a permission for admin and teacher

    def get(self, request):
        detail = Class.objects.all()
        serializer = ClassSerializer(detail,many = True)
        return Response(serializer.data)

class AssignClassRegister(APIView):
    permission_classes = [IsAdmin] # set a permission for admin
    
    def post(self, request): #assign class only by admin
        serializer = AssignClassSerializer(data=request.data)
        teacher_id1 = request.data['teacher_id']
        sub_id1 = request.data['subject_id']
        class_id1 = request.data['class_id']

        if str(teacher_id1).isdigit() and str(sub_id1).isdigit() and str(class_id1).isdigit():
            pass
            if Teacher.objects.filter(id = teacher_id1):
                pass
                if Subject.objects.filter(id = sub_id1):
                    pass
                    if Class.objects.filter(id= class_id1):
                        pass
                        if serializer.is_valid(): #validate the data
                            pass
                            serializer.save()   
                            return Response({"message":"class added successfully",
                                    "details":serializer.data})
                        return Response(serializer.errors)
                    return Response({"message":"class id does not exist"})
                return Response({"message":"Subject id does not exist"})
            return Response({"message":"teacher id does not exist"})
        return Response({"message": "id values must be numeric"})

class ViewAssignClass(APIView): #view the assign class
    permission_classes = [IsAdmin| IsTeacher] # set a permission for admin and teacher

    def get(self, request):
        detail = AssignClass.objects.all()
        serializer = AssignClassSerializer(detail,many = True)
        return Response(serializer.data)

class HomeworkRegister(APIView):
    permission_classes = [IsTeacher] # set a permission for teacher

    def get(self, request):
        detail = Homework.objects.all()
        serializer = HomeworkSerializer(detail,many = True)
        return Response(serializer.data)
    
    def post(self, request):
        serializer = HomeworkSerializer(data=request.data)
        sub_id1 = request.data['subject_id']
        class_id1 = request.data['class_id']
        if str(sub_id1).isdigit() and str(class_id1).isdigit():
            pass
            if Subject.objects.filter(id = sub_id1):
                    pass
                    if Class.objects.filter(id= class_id1):
                        pass
                        if serializer.is_valid(): #validate the data
                            pass
                            serializer.save()   
                            return Response({"message":"homework added successfully",
                                    "details":serializer.data})
                        return Response(serializer.errors)
                    return Response({"message":"class id does not exist"})
            return Response({"message":"Subject id does not exist"})
        return Response({"message": "id values must be numeric"})

class NoticeRegister(APIView): # image upload in noticeboard
    permission_classes = [IsAdmin] # set a permission for admin

    def get(self, request):
        detail = Noticeboard.objects.all()
        serializer = NoticeSerializer(detail,many = True)
        return Response(serializer.data)
    
    def post(self, request):
        serializer = NoticeSerializer(data=request.data)
        if serializer.is_valid(): #validate the image
            pass
            serializer.save()   
            return Response({"message":"notice added successfully",
                    "details":serializer.data})
        return Response(serializer.errors)
               
        
class TeacherPage(APIView): #paginated view
     def get(self, request):
       detail = Teacher.objects.all()
       page_size = 1
       paginator1 = PageNumberPagination()
       paginator1.page_size = page_size
       result_page = paginator1.paginate_queryset(detail, request)
       serializer = TeacherSerializer(result_page, many=True)
       return paginator1.get_paginated_response(serializer.data)


class TeacherPage1(generics.ListAPIView): #another method
        queryset = Teacher.objects.all()
        serializer_class = TeacherSerializer
        pagination_class = PageNumberPagination
        #pagination_class = CustomPage
        
reason_check = re.compile(r'^[\.a-zA-Z0-9, ]*$')

class LeaveRequest(APIView): #leave request for teacher
    permission_classes = [IsTeacher] # set a permission for teacher
    
    def post(self, request):
        request_data = {
            "teacheremp_id":request.data['teacheremp_id'],
            "reason": request.data['reason'],
            "date": request.data['date']
        }
        serializer = LeaveSerializer(data=request_data)

        teacher_id = request.data['teacheremp_id']
        reason1 = request.data['reason']
        date1 = request.data['date']
        format = "%Y-%m-%d"
        characters = list(string.ascii_letters + string.digits)
        code1 = ''.join(random.choice(characters) for i in range(6)) #generate the code
        

        if str(teacher_id).isdigit():
            pass
            if Teacher.objects.filter(teacheremp_id = teacher_id):
                a = Teacher.objects.filter(teacheremp_id = teacher_id).first()
                #print("aa",a.id)
                request_data1 = {
                    "teacheremp_id":a.id,
                    "reason": request.data['reason'],
                    "date": request.data['date'],
                    "status": "pending",
                    "code" :code1
                }
                serializer=LeaveSerializer(data=request_data1)
                try:
                    if datetime.strptime(date1, format):
                        pass
                        if re.fullmatch(reason_check,reason1):
                            pass
                            if serializer.is_valid():
                                serializer.save()
                               
                                admin_mesg = f"teacher leave request\n teacheremp_id: {teacher_id}\n reason: {reason1}\n date: {date1}\n code: {code1}"
                                teacher_mesg = f"your leave request detail\n reason: {reason1}\n date: {date1}\n code: {code1}"
                                admin_mail = Admin.objects.filter(id = 1).first()
                                a_mail = admin_mail.email
                                teacher_mail = a.email
                                send_mail(a_mail,admin_mesg) #send a mail to admin
                                send_mail(teacher_mail,teacher_mesg) #send a mail to teacher
                                return Response({"message":"leave request recorded successfully",})
                            return Response(serializer.errors)
                        return Response({"message": "reason may not be blank and alphabets, numbers, spaces, full stop and comma only allowed"})
                except:
                    return Response({"message": "date must be in this format (yyyy-m-d)"})
            return Response({"message":"invalid teacher id"})
        return Response({"message":"teacher id must be numeric"}) 

class LeaveAllView(APIView): #view all leave details
    permission_classes = [IsAdmin ] # set a permission for admin

    def get(self, request):
        details = Leave.objects.all()
        serializer = LeaveSerializer(details, many = True)
        return Response(serializer.data)

class LeaveView(APIView): #leave status view for teacher
    permission_classes = [IsTeacher] # set a permission for admin and teacher

    def get(self, request,pk):
        try:
            Leave.objects.get(code = pk)
            detail = Leave.objects.filter(code = pk).first()
            return Response({"status": detail.status})
        except:
            return Response({"message": "invalid code"})

class LeaveUpdate(APIView): #leave status update only by admin
    permission_classes = [IsAdmin] # set a permission for admin

    def put(self, request,pk):        
        try:
            Leave.objects.get(code = pk) #get the code
        except:
            return Response({"message": "invalid code"})
        req_data = {
            "status":request.data['status']
                }
        status1 = request.data['status']
        status2 = ['approved', 'cancelled', 'hold']
        if status1 in status2:
            detail = Leave.objects.filter(code = pk).update(status=status1) #update the status
            return Response({"message": "leave permission updated successfully"})
        return Response({"message":"status field only allow 'approved', 'cancelled', 'hold'"})
        
        

























