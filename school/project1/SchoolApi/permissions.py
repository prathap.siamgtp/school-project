from rest_framework.permissions import  BasePermission,  SAFE_METHODS
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied
from .models import Admin


class IsAdmin(BasePermission): #set permission for admin

    # def has_permission(self, request, view):
    #     if request.user.is_superuser:
    #         return True
    #     return False

    # def has_object_permission(self, request, view, obj):
    #     if request.user.is_superuser:
    #         return True
    #     return False
    def has_permission(self, request, view):
        try:
            if Admin.objects.filter(name = request.user):
                a = Admin.objects.filter(name = request.user).first()
                return True
               
        except: 
            raise PermissionDenied({ "detail": "You do not have permission to perform this action."})
           
        # if request.user.username == a.name:
        #     print("ok")
        #     return True
        # return False



    # def has_object_permission(self, request, view, obj):
    #     print("aa",request.user)
    #     if request.method in SAFE_METHODS :
    #         return True
            
    #     return obj.admin('name') == request.user

class IsTeacher(BasePermission): #set permission for teacher

    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff:
            return True
        return False

 